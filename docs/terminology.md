# Terminology
List of commonly used terms.

## Recordable
A model implementing the `Altek\Accountant\Contracts\Recordable` interface. Changes to these models will be recorded.

## Ledger
A record holding the state of a `Recordable` model at a given point in time.
